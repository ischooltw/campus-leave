var gulp = require('gulp');
var replace = require('gulp-replace');
var vulcanize = require('gulp-vulcanize');
var crisper = require('gulp-crisper');
var connect = require('gulp-connect');
var shell = require('gulp-shell');
var minifyInline = require('gulp-minify-inline');

gulp.task('build', function() {
    gulp.src('temp/DCBD39BA-E517-4353-9608-CB8AA3052913/*')
        .pipe(gulp.dest('dist/DCBD39BA-E517-4353-9608-CB8AA3052913'));

    gulp.src('campus-leave-for-statutory-reasons.html')
        .pipe(replace(/<link rel="import" href="../g, '<link rel="import" href="../../bower_components'))
        .pipe(gulp.dest('dist/DCBD39BA-E517-4353-9608-CB8AA3052913'))
        .pipe(replace(/<script src="../g, '<script src="../../bower_components'))
        .pipe(gulp.dest('dist/DCBD39BA-E517-4353-9608-CB8AA3052913'))
        .pipe(replace(/campus-behavior.html/g, 'campus-develope.html'))
        .pipe(gulp.dest('dist/DCBD39BA-E517-4353-9608-CB8AA3052913'))
        .pipe(vulcanize({
            inlineScripts: true,
            inlineCss: true,
            stripComments: true
        }))
        .pipe(minifyInline())
        .pipe(crisper())
        .pipe(gulp.dest('dist/DCBD39BA-E517-4353-9608-CB8AA3052913'));

    gulp.src('temp/9E071C65-6EB9-420F-99F6-6407602B2D4B/*')
        .pipe(gulp.dest('dist/9E071C65-6EB9-420F-99F6-6407602B2D4B'));

    gulp.src('campus-leave-for-teacher.html')
        .pipe(replace(/<link rel="import" href="../g, '<link rel="import" href="../../bower_components'))
        .pipe(gulp.dest('dist/9E071C65-6EB9-420F-99F6-6407602B2D4B'))
        .pipe(replace(/<script src="../g, '<script src="../../bower_components'))
        .pipe(gulp.dest('dist/9E071C65-6EB9-420F-99F6-6407602B2D4B'))
        .pipe(replace(/campus-behavior.html/g, 'campus-develope.html'))
        .pipe(gulp.dest('dist/9E071C65-6EB9-420F-99F6-6407602B2D4B'))
        .pipe(vulcanize({
            inlineScripts: true,
            inlineCss: true,
            stripComments: true
        }))
        .pipe(minifyInline())
        .pipe(crisper())
        .pipe(gulp.dest('dist/9E071C65-6EB9-420F-99F6-6407602B2D4B'));

    gulp.src('temp/3AD1FEBC-DE61-44C0-934C-CED000A9B64B/*')
        .pipe(gulp.dest('dist/3AD1FEBC-DE61-44C0-934C-CED000A9B64B'));

    return gulp.src('campus-leave-for-student.html')
        .pipe(replace(/<link rel="import" href="../g, '<link rel="import" href="../../bower_components'))
        .pipe(gulp.dest('dist/3AD1FEBC-DE61-44C0-934C-CED000A9B64B'))
        .pipe(replace(/<script src="../g, '<script src="../../bower_components'))
        .pipe(gulp.dest('dist/3AD1FEBC-DE61-44C0-934C-CED000A9B64B'))
        .pipe(replace(/campus-behavior.html/g, 'campus-develope.html'))
        .pipe(gulp.dest('dist/3AD1FEBC-DE61-44C0-934C-CED000A9B64B'))
        .pipe(vulcanize({
            inlineScripts: true,
            inlineCss: true,
            stripComments: true
        }))
        .pipe(minifyInline())
        .pipe(crisper())
        .pipe(gulp.dest('dist/3AD1FEBC-DE61-44C0-934C-CED000A9B64B'));
});

gulp.task('serve', function() {
    connect.server({
        root: 'dist'
    });
});

gulp.task('default', ['serve', 'build'], shell.task([
    /^win/.test(require('os').platform()) ? 'start http://localhost:8080/' : 'open http://localhost:8080/'
]));
